<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Camera;
use Illuminate\Http\Request;
use MongoDB\Laravel\Eloquent\Builder;

class CameraController extends Controller
{
    public function show(Camera $camera) {
        return response()->json($camera);
    }

    public function index(Request $request)
    {
        $priceRange = $request->input('price_range');
        $maxIso = $request->input('max_iso');
        $megapixels = $request->input('megapixels');

        $cameras = Camera::query()
            ->select([
                'id',
                'brand',
                'model',
                'megapixels',
                'max_iso',
                'lcd_size',
                'price',
                'weight',
                'battery_life',
            ])
            ->when($priceRange, function (Builder $query) use ($priceRange) {
                $minPrice = min($priceRange);
                $maxPrice = max($priceRange);

                return $query->where('price', '>=', (int) $minPrice)
                    ->where('price', '<=', (int)$maxPrice);
            })
            ->when($maxIso, function (Builder $query) use ($maxIso) {
                return $query->where('max_iso', '<=', (int) $maxIso);
            })
            ->when($megapixels, function ($query) use ($megapixels) {
                return $query->where('megapixels', '>=', (float) $megapixels);
            })
            ->get();

        if ($cameras->isEmpty()) {
            return response()->json();
        }

        // Normalisasi Matriks Keputusan
        $matrix = $cameras->map(function ($camera) {
            return [
                $camera->megapixels,
                $camera->max_iso,
                $camera->lcd_size,
                1 / $camera->price, // Harga dianggap sebagai biaya
                1 / $camera->weight, // Berat dianggap sebagai biaya
                $camera->battery_life
            ];
        })->toArray();

        $normMatrix = $this->normalize($matrix);

        // Bobot Kriteria
        $weights = [0.2, 0.15, 0.15, 0.25, 0.1, 0.15];

        $weightedMatrix = $this->weightedMatrix($normMatrix, $weights);

        $idealSolutions = $this->idealSolutions($weightedMatrix);
        $distances = $this->distances($weightedMatrix, $idealSolutions['idealPositive'], $idealSolutions['idealNegative']);
        $preferenceValues = $this->preferenceValues($distances['distancesPositive'], $distances['distancesNegative']);

        // Gabungkan nilai preferensi dengan data kamera
        $recommendedCameras = [];
        foreach ($cameras as $idx => $camera) {
            $camera->preference_value = $preferenceValues[$idx];
            $recommendedCameras[] = $camera;
        }

        usort($recommendedCameras, function ($a, $b) {
            return $b->preference_value <=> $a->preference_value;
        });

//        $recommendedCameras = $cameras->map(function ($camera, $index) use ($preferenceValues) {
//            $camera->preference_value = $preferenceValues[$index];
//            return $camera;
//        })->sortByDesc('preference_value');

        return response()->json($recommendedCameras);
    }

    private function normalize($matrix)
    {
        $normMatrix = [];
        foreach ($matrix[0] as $key => $value) {
            $column = array_column($matrix, $key);
            $sum = sqrt(array_sum(array_map(function ($val) {
                return pow($val, 2);
            }, $column)));
            foreach ($matrix as $index => $row) {
                $normMatrix[$index][$key] = $row[$key] / $sum;
            }
        }
        return $normMatrix;
    }

    private function weightedMatrix($normMatrix, $weights)
    {
        foreach ($normMatrix as $index => $row) {
            foreach ($row as $key => $value) {
                $normMatrix[$index][$key] = $value * $weights[$key];
            }
        }
        return $normMatrix;
    }

    private function idealSolutions($weightedMatrix)
    {
        $idealPositive = [];
        $idealNegative = [];
        foreach ($weightedMatrix[0] as $key => $value) {
            $column = array_column($weightedMatrix, $key);
            $idealPositive[$key] = max($column);
            $idealNegative[$key] = min($column);
        }
        return compact('idealPositive', 'idealNegative');
    }

    private function distances($weightedMatrix, $idealPositive, $idealNegative)
    {
        $distancesPositive = [];
        $distancesNegative = [];
        foreach ($weightedMatrix as $index => $row) {
            $sumPositive = 0;
            $sumNegative = 0;
            foreach ($row as $key => $value) {
                $sumPositive += pow($value - $idealPositive[$key], 2);
                $sumNegative += pow($value - $idealNegative[$key], 2);
            }
            $distancesPositive[$index] = sqrt($sumPositive);
            $distancesNegative[$index] = sqrt($sumNegative);
        }
        return compact('distancesPositive', 'distancesNegative');
    }

    private function preferenceValues($distancesPositive, $distancesNegative)
    {
        $preferenceValues = [];
        foreach ($distancesPositive as $index => $distancePositive) {
            $sumDistances = $distancePositive + $distancesNegative[$index];
            if ($sumDistances == 0) {
                $preferenceValues[$index] = 0; // atau nilai default lainnya
            } else {
                $preferenceValues[$index] = $distancesNegative[$index] / $sumDistances;
            }
        }
        return $preferenceValues;
    }
}
