<?php

namespace App\Console\Commands;

use App\Models\Camera;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class InsertCamera extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:insert-camera';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $cameraFile = File::get(storage_path('camera.json'));
        $cameraJson = json_decode($cameraFile, true);
        foreach ($cameraJson as $camera) {
            $this->info("Insert camera: {$camera['brand']} {$camera['model']}");
            Camera::create($camera);
        }

        $this->info('Done.');
    }
}
