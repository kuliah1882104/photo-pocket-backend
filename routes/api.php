<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CameraController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get('/cameras', [CameraController::class, 'index']);
    Route::get('/cameras/{camera}', [CameraController::class, 'show']);
});
